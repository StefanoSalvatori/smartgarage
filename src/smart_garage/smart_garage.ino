#include "ButtonImpl.h"
#include "LedExt.h"
#include "Task.h"
#include "DoorTask.h"
#include "Scheduler.h"
#include "Button.h"
#include "ParkAssistant.h"
#include "Pir.h"
#include "SerialTask.h"
#include "Config.h"
#include "Context.h"

#define SCHED_TIME 40
#define DOOR_TSK_PERIOD 120
#define PARK_TSK_PERIOD 120
#define SERIAL_TSK_PERDIOD 120

Scheduler sched;

void setup() {
  Serial.begin(9600);
  
  Button* touchButton = new ButtonImpl(TOUCH_BTN_PIN);
  Button* closeButton = new ButtonImpl(CLOSE_BTN_PIN);
  LedExt* fadingLed = new LedExt(DOOR_LED_PIN);
  LedExt* led1 = new LedExt(LED1_PIN);
  LedExt* led2 = new LedExt(LED2_PIN);
  Context* context = new Context(new NewPing(TRIG,ECHO,MAX_DISTANCE), new Pir(PIR_PIN));

  DoorTask* door = new DoorTask(closeButton, fadingLed, context);  
  ParkAssistant* park = new ParkAssistant(touchButton, led1, led2, context);
  
  door->init(DOOR_TSK_PERIOD);
  park->init(PARK_TSK_PERIOD);
  SerialTask::get()->init(SERIAL_TSK_PERDIOD);

  sched.init(SCHED_TIME);  
  sched.addTask(SerialTask::get());
  sched.addTask(door);
  sched.addTask(park);
  
  Serial.println("Calibrazione Sensori, attendere...");
  delay(5000);
  Serial.println("Ready!");
}

void loop() {
    sched.run();
}

