#ifndef __PARKASSISTANT__
#define __PARKASSISTANT__

#include "Task.h"
#include "LightExt.h"
#include "Button.h"
#include "Context.h"

class ParkAssistant : public Task {

private:
  LightExt* fadingLed1;
  LightExt* fadingLed2;
  Context* context;
  Button* touchButton;
  enum { IDLE, PARKING } state;

public:
  ParkAssistant(Button* button, LightExt* fadingLed1, LightExt* fadingLed2, Context* context);
  void init(int period);
  void tick();
};

#endif
