#ifndef __CONFIG__
#define __CONFIG__


#define DOOR_LED_PIN 3
#define LED1_PIN 6
#define LED2_PIN 5
#define TOUCH_BTN_PIN 7 
#define CLOSE_BTN_PIN 8 
#define PIR_PIN 10
#define ECHO 11
#define TRIG 12
#define MAX_DISTANCE 400


#endif
 
