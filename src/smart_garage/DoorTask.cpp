#include "DoorTask.h"
#include "Context.h"
#include "Arduino.h"
#include "LedExt.h"
#include "Button.h"
#include "SerialTask.h"


#define DOOR_OPENING_TIME 2000
#define OPEN_TIME 10000
#define FADE_AMOUNT (LedExt::MAX_INT) * (this->getPeriod()) / DOOR_OPENING_TIME //Valore per far si che il fading duri 2 secondi
#define CAN_CLOSE (!(context->presenceDetected()) || currentDistance < DIST_CLOSE) //Vero se la macchina non ostruisce la chiusura della porta

/**
  Procedura di fading per il led.
*/
void fading(LightExt* led, int fadeAmount) {
  int newIntensity = led->getIntensity() + fadeAmount;
  led->setIntensity(newIntensity);
}


DoorTask::DoorTask(Button* closeButton, LightExt* fadingLed, Context* context) {
  this->fadingLed = fadingLed;
  this->context = context;
  this->closeButton = closeButton;
}

void DoorTask::init(int period) {
  Task::init(period);
  state = CLOSE;
}

void DoorTask::tick() {
  static unsigned long time_t = 0;
  unsigned long currentDistance =  context->currentDistance();
  unsigned int t = 0;
  boolean presDetected;
  SerialTask* serial = SerialTask::get();
  switch (state) {
    case CLOSE:
      if (serial->read() == 'o') {
        this->fadingLed->switchOn();
        this->fadingLed->setIntensity(0);
        state = OPENING;
      }
      break;
    case OPENING:
      fading(this->fadingLed, FADE_AMOUNT);
      if (this->fadingLed->getIntensity() >= LedExt::MAX_INT) {
        this->fadingLed->setIntensity(LedExt::MAX_INT);
        context->setDoorOpen(true);
        time_t = millis();
        state = OPEN_WAITING_CAR;
      }
      if (this->closeButton->isPressed() && CAN_CLOSE) {
        state = CLOSING;
      }
      break;
    case OPEN_WAITING_CAR:
      if (context->presenceDetected()) {
        state = OPEN_CAR_ARRIVED;
      } else if (millis() - time_t > OPEN_TIME || this->closeButton->isPressed()) {
        context->setDoorOpen(false);
        state = CLOSING;
      }
      break;
    case OPEN_CAR_ARRIVED:
      if (((serial->read() == 's' && currentDistance < DIST_MIN) || (this->closeButton->isPressed() && CAN_CLOSE))) {
        context->setDoorOpen(false);
        state = CLOSING;
      }
      break;
    case CLOSING:
      fading(this->fadingLed, -FADE_AMOUNT);
      if (this->fadingLed->getIntensity() <= 0) {
        this->fadingLed->setIntensity(0);
        this->fadingLed->switchOff();
        state = CLOSE;
      }
      if (serial->read() == 'o' || !CAN_CLOSE) {
        state = OPENING;
      }
      break;
  }
}




