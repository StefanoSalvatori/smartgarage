#include "SerialTask.h"
#include "Arduino.h"



SerialTask* SerialTask::INSTANCE = 0;

SerialTask* SerialTask::get() {
  //Lazy initialization
	if (INSTANCE == 0) {
		INSTANCE = new SerialTask();
	}
	return INSTANCE;
}

char SerialTask::read() {
	return this->readBuffer;
}

void SerialTask::write(String s){
  this->writeBuffer[size++] = s;
}

void SerialTask::init(int period) {
	Task::init(period);
  
}

SerialTask::SerialTask() {
  this->readBuffer = '?';
  size = 0;
}

/**
 * Ogni tick legge dalla seriale e svuota il buffer di scrittura.
 */
void SerialTask::tick() {
    this->readBuffer = Serial.available() ? Serial.read() : '?';
    for(int i = 0; i<size ; i++){
      Serial.println(this->writeBuffer[i]);
      this->writeBuffer[i] = "";
    }
    size = 0;
}
