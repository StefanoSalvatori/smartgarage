#ifndef __DOORTASK__
#define __DOORTASK__

#include "Task.h"
#include "LightExt.h"
#include "Button.h"
#include "Context.h"

class DoorTask: public Task {

private:
  LightExt* fadingLed;
  Context* context;
  Button* closeButton;
  enum { CLOSE, OPENING, OPEN_WAITING_CAR, OPEN_CAR_ARRIVED, CLOSING} state;

public:
  DoorTask(Button* closeButton, LightExt* fadingLed, Context* context);  
  void init(int period);  
  void tick();
};

#endif
