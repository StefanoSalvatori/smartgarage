#include "ParkAssistant.h"
#include "Arduino.h"
#include "LedExt.h"
#include "Pir.h"
#include "ButtonImpl.h"
#include "SerialTask.h"
#include "Context.h"


/**
 * Gestisce l'intensità dei due led in base al valore dist.
 */
void setLeds(int dist , LightExt* led1, LightExt* led2){
  if( dist>=DIST_MAX) {
    led1->switchOff();
    led2->switchOff();
    } else if (dist < DIST_MAX && dist >= DIST_MIN) {
      led2->switchOff();
      led1->setIntensity(map(dist, DIST_MAX-DIST_MIN,0 , 0, LedExt::MAX_INT));
      led1->switchOn();
    } else if (dist < DIST_MIN) {
      led1->switchOn(); 
      led1->setIntensity(LedExt::MAX_INT);
      led2->setIntensity(map(dist, DIST_MIN, 0, 0, LedExt::MAX_INT));
      led2->switchOn(); 
    }
}

ParkAssistant::ParkAssistant(Button* button, LightExt* fadingLed1, LightExt* fadingLed2, Context* context) {
  this->fadingLed1 = fadingLed1;
  this->fadingLed2 = fadingLed2;
  this->context = context;
  this->touchButton = button;
}

void ParkAssistant::init(int period) {
  Task::init(period);
  this->state = IDLE;
}

void ParkAssistant::tick() {
  int readFromSerial;
  unsigned int currentDistance;
  static unsigned int prevDistance = DIST_MAX; //Per non stampare continuamente le stesse informazioni
  bool buttonPressed;
  SerialTask* serial = SerialTask::get();
  
  switch (state)
  {
  case IDLE:
    if (context->presenceDetected() && context->isDoorOpen()) {
      serial->write(".:Welcome Home:.");
      this->state = PARKING;
    }
    break;
  case PARKING:
    currentDistance =  context->currentDistance();  
    if(prevDistance != currentDistance){
      serial->write("Distance:" + String(currentDistance));
    }
    
    setLeds(currentDistance,  this->fadingLed1, this->fadingLed2);
    
    if(currentDistance < DIST_MIN && !(prevDistance < DIST_MIN)){
      serial->write("OK CAN STOP");
    }
    if (this->touchButton->isPressed()) {
      serial->write("TOUCHING");
    }
    
    if (serial->read() == 's') {
      if (currentDistance < DIST_MIN) {
        serial->write("OK");
        this->fadingLed1->switchOff();
        this->fadingLed2->switchOff();
        this->state = IDLE;
      }else {
        serial->write("TOO FAR");
      }
    } 
    prevDistance = currentDistance;   
    break;
  }
}




