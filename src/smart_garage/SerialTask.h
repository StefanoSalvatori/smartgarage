#ifndef __SERIALTASK__
#define __SERIALTASK__

#include "Task.h"
#include "Arduino.h"

#define W_BUFF_SIZE 10
/*
 * Classe singleton che gestisce la comunicazione con la seriale.
 */
class SerialTask : public Task {
private:
	//Singleton
	static SerialTask* INSTANCE;
	char readBuffer;
  String writeBuffer[W_BUFF_SIZE];
  int size;

public:
	static SerialTask* get();
	char read();
  void write(String s);
	void init(int period);
	void tick();

private:
	SerialTask();//costruttore privato
};

#endif



