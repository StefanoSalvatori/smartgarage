#ifndef __PRESENCESENSOR__
#define __PRESENCESENSOR__

class PresenceSensor {

public:
	virtual bool presenceDetected() = 0;
};


#endif
