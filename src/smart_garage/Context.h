#ifndef __CONTEXT__
#define __CONTEXT__

#include "NewPing.h"
#include "PresenceSensor.h"

#define DIST_CLOSE 50
#define DIST_MIN 10
#define DIST_MAX 100

/*
 * Classe per gestire i sensori e le variabili condivise tra i task.
 */
class Context {

public: 
  Context(NewPing* prox, PresenceSensor* pres);
  bool presenceDetected();
  unsigned int currentDistance();
  void setDoorOpen(bool open);
  bool isDoorOpen();

private:
    bool doorOpen;
    NewPing* proximitySensor;
    PresenceSensor* presenceSensor;
};

#endif
