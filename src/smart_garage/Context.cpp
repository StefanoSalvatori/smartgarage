#include "Context.h"

Context::Context(NewPing* prox, PresenceSensor* pres) {
  this->presenceSensor = pres;
  this->proximitySensor = prox;
  this->doorOpen = false;
}

bool Context::presenceDetected() {
  return this->presenceSensor->presenceDetected();
}

unsigned int Context::currentDistance() {
  unsigned int curr = NewPing::convert_cm(this->proximitySensor->ping_median(4, DIST_MAX));
  
  return curr ? curr : DIST_MAX; //La convert_cm() di NewPing torna il valore zero per indicare una 
                                 //distanza maggiore della massima distanza rilevabile.
                                 //Restituiamo in quel caso DIST_MAX
}

void Context::setDoorOpen(bool open) {
  this->doorOpen = open;
}

bool Context::isDoorOpen() {
  return this->doorOpen;
}


