package controller;

import gui.GUI;
import model.SerialCommChannel;

public class ControllerRC implements Controller {

    private GUI gui;
    private SerialCommChannel serial;

    public ControllerRC(String comPortName, int dataRate) throws Exception {

        this.serial = new SerialCommChannel(this, comPortName, dataRate);
        this.gui = new GUI(this);
        this.gui.setVisible(true);

    }

    @Override
    public void notifyOpenCommand() {
        this.serial.sendMsg("o");
    }

    @Override
    public void notifyStopCommand() {
        this.serial.sendMsg("s");
    }

    @Override
    public void notifyNewMessage(String msg) throws InterruptedException {
        this.gui.setText(msg);
        this.gui.repaint();
    }

    public void close() {
        this.serial.close();
    }

}
