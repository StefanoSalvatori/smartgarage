package controller;

/**
 * Command controller.
 */
public interface Controller {

    void notifyOpenCommand();

    void notifyStopCommand();

    void notifyNewMessage(String msg) throws InterruptedException;

    void close();

}
