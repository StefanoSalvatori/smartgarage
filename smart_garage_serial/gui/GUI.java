package gui;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import controller.ControllerRC;

/**
 *
 */
public class GUI extends JFrame {

    private static final long serialVersionUID = 1L;
    private static final int IN = 5;

    private final JPanel panel = new JPanel();
    private final GridBagLayout grid = new GridBagLayout();
    private final GridBagConstraints c = new GridBagConstraints();

    private final JButton openButton = new JButton("Open Garage");
    private final JButton stopButton = new JButton("Stop");

    private final JTextArea display = new JTextArea("", 2, 20);

    /**
     * 
     * @param controller
     *          controller della scena
     */
    public GUI(final ControllerRC controller) {
        super();
        panel.setLayout(grid);
        c.insets = new Insets(IN, IN, IN, IN);
        display.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        display.setEditable(false);
        display.setLineWrap(true);

        openButton.addActionListener(e -> {
            controller.notifyOpenCommand();
        });

        stopButton.addActionListener(e -> {
            controller.notifyStopCommand();
        });

        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(final java.awt.event.WindowEvent windowEvent) {
                if (JOptionPane.showConfirmDialog(GUI.this, "Are you sure to close this window?", "Really Closing?",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                    controller.close();
                    System.exit(0);
                }
            }
        });

        c.gridx = 0;
        c.gridy = 0;

        panel.add(openButton, c);
        c.gridy++;
        panel.add(stopButton, c);
        c.gridy++;
        panel.add(display, c);

        this.add(panel);
        this.pack();
        this.setLocationRelativeTo(null);

    }

    public void setText(final String st) {
        this.display.setText(st);
    }

}
