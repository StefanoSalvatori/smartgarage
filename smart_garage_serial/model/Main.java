package model;

import controller.ControllerRC;

/**
 * Testing simple message passing.
 * 
 * To be used with an Arduino connected via Serial Port running
 * modulo-lab-2.2/pingpong.ino
 * 
 * @author aricci
 *
 */
public class Main {

    public static void main(String[] args) throws Exception {
        /* attesa necessaria per fare in modo che Arduino completi il reboot */

        new ControllerRC("COM6", 9600);
        System.out.println("Waiting Arduino for rebooting...");
        Thread.sleep(4000);
        // System.out.println("Ready.");

    }

}
